import subprocess
import time
import threading
import sys
import shlex

class TrackerThread(threading.Thread):
    def __init__(self, PID, timeout, username=""):
        threading.Thread.__init__(self, args=())
        self.PID = PID
        self.timeout = timeout
        self.daemon = True
        self.username = username
        self.process = None
        print "[+] Creating new thread with strace PID: " + str(self.PID) + " User: " + str(self.username)

    def trackerPass(self, line):
        if "read(" in line and "\\f\\0\\0\\0" in line or "\\v\\0\\0\\0" in line:
            #print line
            if "\\f\\0\\0\\0" in line:
                line = line[line.index("\\f\\0\\0\\0")+10:]
            elif "\\v\\0\\0\\0" in line:
                line = line[line.index("\\v\\0\\0\\0")+10:]
            else:
                print "[*] Warning, line no parsed"
                print line
                return
            line = line[:line.index('", ')]
            print "[*] Data from PID "+ str(self.PID) + ":\n\t" + line

    def run(self):
        cmd = "strace -p " + str(self.PID)
        #print "init process strace"
        self.process = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #os.kill(os.getpgid(process.pid), signal.SIGKILL)
        time_out_strace = time.time() + int(self.timeout)
        #process.wait()
        while True:
            if time.time() > time_out_strace:
                print "[*] Timeout reached, close thread PID: " + str(self.PID)
                try:
                    self.process.kill()
                except:
                    pass
                break
            output = self.process.stderr.readline()
            if output != '':
                #print output
                self.trackerPass(output.strip())



def getProcesses():
    ps_process = subprocess.Popen(["ps", "aux"], stdout=subprocess.PIPE)
    grep_process = subprocess.Popen(["grep", "ssh"], stdin=ps_process.stdout, stdout=subprocess.PIPE)
    ps_process.stdout.close()  # Allow ps_process to receive a SIGPIPE if grep_process exits.
    output = grep_process.communicate()[0]
    return output.split("\n")


def main():
    __banned_pids__ = []
    if len(sys.argv) > 1 and sys.argv[1] == 's':
        os.setuid(0)
        print "[+] Got Root permissions"
    else:
        print "[*] For Python setuid cases use: 'python %s s'" % sys.argv[0]
    # First run to get current procceses and bann running active conections
    print "[+] Get current active conections and get it banned"
    processes = getProcesses()
    for proc in processes:
        if "[priv]" in proc:
            proc_data = proc.split()
            print "[Banned PID]: " + str(proc_data[1])
            __banned_pids__.append(proc_data[1])

    print "[+] Waiting for new conections ..."
    time_out_ot = time.time() + int(25)
    while True:
        time.sleep(0.5)
        if time.time() > time_out_ot:
            sys.stdout.write(".")
            sys.stdout.flush()
            time_out_ot = time.time() + int(25)
        processes = getProcesses()
        for proc in processes:
            if "[priv]" in proc:
                proc_data = proc.split()
                if proc_data[1] not in __banned_pids__:
                    print "[+] New connection detected"
                    if len(proc_data) >= 11:
                        thread_strace = TrackerThread(proc_data[1], 15, proc_data[11]) # set timeout argv 2
                    else:
                        thread_strace = TrackerThread(proc_data[1], 15)
                    thread_strace.start()
                    __banned_pids__.append(proc_data[1])


if __name__ == "__main__":
    main()
